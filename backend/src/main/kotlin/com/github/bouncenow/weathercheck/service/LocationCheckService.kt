package com.github.bouncenow.weathercheck.service

import com.github.bouncenow.weathercheck.model.LocationCheck
import com.github.bouncenow.weathercheck.model.LocationForecastForDate
import com.github.bouncenow.weathercheck.model.LocationView
import com.github.bouncenow.weathercheck.model.TemperatureLimit
import com.github.bouncenow.weathercheck.model.command.CreateLocationCheckCommand
import com.github.bouncenow.weathercheck.model.weatherapi.ForecastResult
import com.github.bouncenow.weathercheck.repository.LocationCheckRepository
import com.github.bouncenow.weathercheck.util.average
import com.github.bouncenow.weathercheck.util.subscribeOnSingleScheduler
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import reactor.core.publisher.toMono
import java.lang.IllegalArgumentException
import java.math.BigDecimal
import java.time.Duration
import java.time.LocalDateTime

@Service
@EnableConfigurationProperties(LocationCheckConfiguration::class)
@EnableScheduling
class LocationCheckService(
        private val repository: LocationCheckRepository,
        private val weatherApiClient: WeatherApiClient,
        private val config: LocationCheckConfiguration
) {

    fun findAll() = repository.findAll()

    fun createAndCheck(command: CreateLocationCheckCommand) =
            LocationCheck(location = command.location, limit = TemperatureLimit(BigDecimal.ZERO, BigDecimal.TEN))
                    .toMono()
                    .flatMap {
                        getForecastAndUpdateLocation(it)
                    }
                    .flatMap { repository.save(it) }
                    .map { it.toView() }

    private fun getForecastAndUpdateLocation(locationCheck: LocationCheck) =
            weatherApiClient.getForecast(locationCheck.location)
                    .map { forecast ->
                        val forecastWithCheckedLimits = forecast.checkLimits(locationCheck.limit)
                        log.debug("Forecast for ${locationCheck.location}: $forecastWithCheckedLimits")
                        locationCheck.withUpdatedForecast(forecastWithCheckedLimits)
                    }
                    .onErrorResume { ex ->
                        log.error("Error during api call for ${locationCheck.id}, ${locationCheck.location}: ${ex.message}")
                        locationCheck.withError().toMono()
                    }

    private fun LocationCheck.toView(): LocationView =
            LocationView(
                    id = id ?: throw IllegalArgumentException("Mapping to view non saved instance of LocationCheck"),
                    location = location,
                    limit = limit,
                    lastChecked = lastChecked,
                    lastError = lastError,
                    errorsExceededMaxErrorCount = errorCount >= config.maxErrorCount,
                    forecasts = forecasts.sortedBy { it.date }
            )

    @Scheduled(fixedRateString = "\${location-check.scheduled-task-interval}")
    fun checkAllNeeded() {
        val now = LocalDateTime.now()

        val checkedSince = now.minus(config.checkInterval)
        val errorSince = now.minus(config.checkSinceErrorInterval)
        repository.findLocationsNeededToCheck(checkedSince, errorSince, config.maxErrorCount)
                .flatMap { getForecastAndUpdateLocation(it) }
                .flatMap { repository.save(it) }
                .then()
                .subscribeOnSingleScheduler()
    }

    private fun ForecastResult.checkLimits(limit: TemperatureLimit) =
            weatherData
                    .groupBy { it.time.toLocalDate() }
                    .map { (date, temperatures) ->
                        val temperaturesWhereLimitExceeded = temperatures.filter {
                            it.temperatureInfo.temperature !in limit
                        }
                        val temperature = if (temperaturesWhereLimitExceeded.isNotEmpty()) {
                            temperaturesWhereLimitExceeded.first().temperatureInfo.temperature
                        } else {
                            temperatures.map { it.temperatureInfo.temperature }.average()
                        }
                        LocationForecastForDate(
                                date = date,
                                limitExceeded = temperaturesWhereLimitExceeded.isNotEmpty(),
                                temperature = temperature
                        )
                    }

    companion object {
        private val log = LoggerFactory.getLogger(LocationCheckService::class.java)
    }

}

@ConfigurationProperties("location-check")
class LocationCheckConfiguration {
    lateinit var checkInterval: Duration
    lateinit var checkSinceErrorInterval: Duration
    var maxErrorCount: Int = 3
}