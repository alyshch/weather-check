package com.github.bouncenow.weathercheck.model

import java.time.LocalDateTime

data class LocationView(
        val id: String,
        val location: CityWithCountryCode,
        val limit: TemperatureLimit,
        val lastChecked: LocalDateTime?,
        val lastError: LocalDateTime?,
        val errorsExceededMaxErrorCount: Boolean,
        val forecasts: List<LocationForecastForDate>
)