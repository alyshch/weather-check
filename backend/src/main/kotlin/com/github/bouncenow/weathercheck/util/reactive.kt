package com.github.bouncenow.weathercheck.util

import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers

fun <T> Mono<T>.subscribeOnSingleScheduler() = subscribeOn(Schedulers.single())
        .subscribe()