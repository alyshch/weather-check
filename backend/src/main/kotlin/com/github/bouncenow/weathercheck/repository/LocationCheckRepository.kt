package com.github.bouncenow.weathercheck.repository

import com.github.bouncenow.weathercheck.model.LocationCheck
import org.springframework.data.mongodb.repository.Query
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux
import java.time.LocalDateTime

interface LocationCheckRepository : ReactiveMongoRepository<LocationCheck, String> {

    /**
     * Find locations which have error count less or equal to given, and either:
     * - no errors (lastError is null), and lastChecked is either null or greater than given
     * - lastError is greater or equal to given.
     */
    @Query("{ errorCount: {\$lt: ?2}, \$or: [ {\$or: [{lastChecked: null}, {lastChecked: {\$lte: ?0}}], lastError: null}, {lastError: {\$lte: ?1}}]} ] }")
    fun findLocationsNeededToCheck(
            lastCheckedSince: LocalDateTime,
            lastErrorSince: LocalDateTime,
            maxErrorCount: Int
    ): Flux<LocationCheck>

}