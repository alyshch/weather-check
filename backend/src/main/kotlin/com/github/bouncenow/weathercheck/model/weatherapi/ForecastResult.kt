package com.github.bouncenow.weathercheck.model.weatherapi

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.github.bouncenow.weathercheck.util.LocalDateTimeFromEpochDeserializer
import java.math.BigDecimal
import java.time.LocalDateTime

data class ForecastResult(
        @JsonProperty("list") val weatherData: List<WeatherData>
) {
    data class WeatherData(
        @JsonProperty("dt")
        @JsonDeserialize(using = LocalDateTimeFromEpochDeserializer::class)
        val time: LocalDateTime,
        @JsonProperty("main") val temperatureInfo: TemperatureInfo
    )

    data class TemperatureInfo(
            @JsonProperty("temp") val temperature: BigDecimal
    )
}