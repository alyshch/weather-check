package com.github.bouncenow.weathercheck.util

import java.math.BigDecimal

fun List<BigDecimal>.average(): BigDecimal =
        if (isEmpty()) {
            BigDecimal.ZERO
        } else {
            fold(BigDecimal.ZERO) { l, r -> l + r } / size.toBigDecimal()
        }