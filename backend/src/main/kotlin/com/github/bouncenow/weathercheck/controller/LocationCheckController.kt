package com.github.bouncenow.weathercheck.controller

import com.github.bouncenow.weathercheck.model.command.CreateLocationCheckCommand
import com.github.bouncenow.weathercheck.service.LocationCheckService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/locationChecks")
class LocationCheckController(
        private val locationCheckService: LocationCheckService
) {

    @PostMapping
    fun create(@RequestBody command: CreateLocationCheckCommand) =
            locationCheckService.createAndCheck(command)

    @GetMapping
    fun findAll() = locationCheckService.findAll()

}