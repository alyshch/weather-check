package com.github.bouncenow.weathercheck.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.IndexDirection
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime

@Document
data class LocationCheck(
        @Id val id: String? = null,
        val location: CityWithCountryCode,
        val limit: TemperatureLimit,
        @Indexed(direction = IndexDirection.DESCENDING)
        val lastChecked: LocalDateTime? = null,
        @Indexed(direction = IndexDirection.DESCENDING)
        val lastError: LocalDateTime? = null,
        val errorCount: Int = 0,
        val forecasts: List<LocationForecastForDate> = emptyList()
) {

    fun withUpdatedForecast(forecasts: List<LocationForecastForDate>) = copy(
            forecasts = forecasts,
            lastChecked = LocalDateTime.now(),
            lastError = null,
            errorCount = 0
    )

    fun withError() = copy(errorCount = errorCount + 1, lastError = LocalDateTime.now())
}

data class CityWithCountryCode(
        val city: String,
        val countryCode: String
)

data class TemperatureLimit(
        override val start: BigDecimal,
        override val endInclusive: BigDecimal
): ClosedRange<BigDecimal>

data class LocationForecastForDate(
        val date: LocalDate,
        val limitExceeded: Boolean,
        val temperature: BigDecimal
)