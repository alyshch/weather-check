package com.github.bouncenow.weathercheck.service

import com.github.bouncenow.weathercheck.model.CityWithCountryCode
import com.github.bouncenow.weathercheck.model.weatherapi.ForecastResult
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono

@Service
@EnableConfigurationProperties(WeatherApiConfiguration::class)
class WeatherApiClient(
        private val config: WeatherApiConfiguration
) {

    private val client = WebClient.create(config.url)

    fun getForecast(location: CityWithCountryCode): Mono<ForecastResult> =
            client
                    .get()
                    .uri { builder ->
                        builder.path(FORECAST_URI_PATH)
                                .queryParam(LOCATION_QUERY_PARAM, location.buildQueryParam())
                                .queryParam(METRIC_QUERY_PARAM, METRIC_UNITS)
                                .queryParam(API_KEY_QUERY_PARAM, config.apiKey)
                                .build()
                    }
                    .retrieve()
                    .bodyToMono()

    private fun CityWithCountryCode.buildQueryParam() = "$city,$countryCode"

    companion object {
        private const val FORECAST_URI_PATH = "/data/2.5/forecast"
        private const val API_KEY_QUERY_PARAM = "APPID"
        private const val LOCATION_QUERY_PARAM = "q"
        private const val METRIC_QUERY_PARAM = "units"
        private const val METRIC_UNITS = "metric"
    }

}

@ConfigurationProperties("openweathermap")
class WeatherApiConfiguration {
    lateinit var url: String
    lateinit var apiKey: String
}