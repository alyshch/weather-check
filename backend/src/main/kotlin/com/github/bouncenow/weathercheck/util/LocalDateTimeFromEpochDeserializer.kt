package com.github.bouncenow.weathercheck.util

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import java.io.IOException
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.time.*


class LocalDateTimeFromEpochDeserializer : StdDeserializer<LocalDateTime>(LocalDate::class.java) {

    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): LocalDateTime {
        return Instant.ofEpochSecond(jp.readValueAs(Long::class.java)).atOffset(ZoneOffset.UTC).toLocalDateTime()
    }

}
