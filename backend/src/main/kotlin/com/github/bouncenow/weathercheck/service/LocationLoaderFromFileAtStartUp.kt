package com.github.bouncenow.weathercheck.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.bouncenow.weathercheck.model.command.CreateLocationCheckCommand
import com.github.bouncenow.weathercheck.util.subscribeOnSingleScheduler
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import reactor.core.publisher.toFlux
import reactor.core.publisher.toMono
import java.nio.file.Files
import java.nio.file.Paths

@Component
@EnableConfigurationProperties(LocationLoaderFromFileConfig::class)
class LocationLoaderFromFileAtStartUp(
        private val locationCheckService: LocationCheckService,
        private val config: LocationLoaderFromFileConfig,
        private val objectMapper: ObjectMapper
) : CommandLineRunner {

    override fun run(vararg args: String?) {
        val pathToLocationsFile = config.pathToLocationsFile
        if (pathToLocationsFile == null) {
            log.debug("Path to file with locations not set, skipping")
            return
        }

        // although reading a file is a blocking operation, it happens only once at start-up
        pathToLocationsFile
                .toMono()
                .map {
                    Files.readAllBytes(Paths.get(it))
                }
                .flatMapMany {
                    objectMapper.readValue<List<CreateLocationCheckCommand>>(it)
                            .toFlux()
                }
                .flatMap {
                    locationCheckService.createAndCheck(it)
                }
                .then()
                .subscribeOnSingleScheduler()
    }

    companion object {
        private val log = LoggerFactory.getLogger(LocationLoaderFromFileAtStartUp::class.java)
    }

}

@ConfigurationProperties("location-loader-from-file")
class LocationLoaderFromFileConfig {

    var pathToLocationsFile: String? = null

}