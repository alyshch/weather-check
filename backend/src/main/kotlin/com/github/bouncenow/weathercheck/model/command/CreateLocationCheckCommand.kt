package com.github.bouncenow.weathercheck.model.command

import com.github.bouncenow.weathercheck.model.CityWithCountryCode
import com.github.bouncenow.weathercheck.model.TemperatureLimit

data class CreateLocationCheckCommand(val location: CityWithCountryCode, val limit: TemperatureLimit)