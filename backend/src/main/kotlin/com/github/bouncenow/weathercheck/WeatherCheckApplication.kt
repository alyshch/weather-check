package com.github.bouncenow.weathercheck

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WeatherCheckApplication

fun main(args: Array<String>) {
	runApplication<WeatherCheckApplication>(*args)
}

