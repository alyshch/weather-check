#Backend:
To build, run `./gradlew build` in `backend` folder. Then, app can be run if JDK8 is installed: `java -jar build/libs/${jar_name}`.
###Config:
* environment variable WEATHERMAP_API_KEY should be present in app environment, which contains API_KEY to https://openweathermap.org/api
* examples of other config options can be found in src/main/resources/application.yml. 
    To customize config, application.yml file with needed config options can be placed beside .jar file, 
    or path to config file can be set via --spring.config.location command line option.
###Functionality
App periodically checks for 5 day forecast for provided location via `openweathermap.org` API. 
Period, with which checks are performed is given by config option `location-check.check-interval`. 
To submit a new location to check, you can POST to `/locationChecks`.
Objects of following form are accepted:
```typescript
{
    location: {
        city: string,
        countryCode: String
    },
    limit: {
        start: number,
        end: number
    }
}
```
For example POST-ing following entity:
```
{
    "location": {
        "city": "London",
        "countryCode": "gb"
    },
    "limit": {
        "start": -10,
        "end": 10
    }
}
```
will check temperature forecast for London, GB, and a check will be performed that they're inside the given limits.
GET-ting endpoint `/locationChecks` will give states of submitted locations checks. (There should be a some kind of a pagination, of course, but I haven't had time for that).
Also, at the startup, application will try to load location checks from `json` file given by config option `location-loader-from-file.path-to-locations-file`,
in this file json array of objects of type given above is expected, and all these objects will be added for periodic checks.

##Design
All the location checks objects are stored in MongoDB. Every `LocationCheck` is stored with the last successful time check, and number of continuos errors during API calls.
Task (via Spring scheduling) is scheduled for short interval. Every time it will check for `LocationChecks` which have last check time earlier than `location-check.check-interval` before current time.
`LocationCheck`s which have error count more than max configured value, will be skipped.
MongoDB is used since 1) app model, even if hello-world-y, fits nicely to Mongo document model, 2) no pre-defined schema allows for fast changes at the start 3) Mongo's reactive API plays nicely with reactive model of 
Spring WebFlux.
WebFlux is used since it's reactive and that allows better utilization of threads, since our app mostly waits for HTTP requests and requests to Mongo.

#Frontend
Frontend is React app written using Create React App. You can run app locally via dev web server using
```
cd frontend
npm install
npm start
```
Property `proxy` in `package.json` file can be set for address of backend API (currently app does requests agains local url-s, so that requests will be proxied to real instance of a backend).