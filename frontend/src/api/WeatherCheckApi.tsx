import axios, {AxiosPromise} from 'axios';

import { LocationView, CreateLocationCheckCommand } from '../model/Location';

export function getLocationViews(): AxiosPromise<LocationView[]> {
    return axios.get('/locationChecks');
}

export function createLocationCheck(command: CreateLocationCheckCommand): AxiosPromise<LocationView> {
    return axios.post('/locationChecks', command);
}