import * as React from 'react';
import { Switch, Route, Redirect } from 'react-router';

import 'bulma/css/bulma.css';

import { Container, Section } from 'bloomer';

import Navigation from './components/Navigation';
import { Locations } from './components/Locations';
import CreateLocationForm from './components/CreateLocationForm';

export const App = () => (
    <React.Fragment>
        <Navigation />
        <Section>
            <Container>
                <Main />
            </Container>
        </Section>
    </React.Fragment>
);

const Main = () => (
    <Switch>
        <Route path="/locations/create">
            <CreateLocationForm/>
        </Route>
        <Route path="/locations">
            <Locations />
        </Route>
        <Route path="/"><Redirect to="/locations" /></Route>
    </Switch>
);