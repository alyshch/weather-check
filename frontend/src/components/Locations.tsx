import * as React from 'react';
import axios from 'axios';

import { ComponentState } from '../util/Utils';
import { LocationView } from '../model/Location';
import { getLocationViews } from '../api/WeatherCheckApi';
import { LocationList } from './LocationList';
import { Box } from 'bloomer/lib/elements/Box';
import { Notification } from 'bloomer/lib/elements/Notification';
import { Columns } from 'bloomer/lib/grid/Columns';
import { Column } from 'bloomer/lib/grid/Column';
import { Button } from 'bloomer/lib/elements/Button';

interface LocationsState {
    locations: LocationView[],
    state: ComponentState,
}

export class Locations extends React.Component<{}, LocationsState> {

    constructor(props: {}) {
        super(props);

        this.state = { locations: [], state: ComponentState.NOT_LOADING }
    }

    componentDidMount() {
        this.updateLocations();
    }

    updateLocations = () => {
        this.setState({ state: ComponentState.LOADING })
        getLocationViews()
            .then(res => {
                const locations = res.data;
                this.setState({ locations, state: ComponentState.NOT_LOADING });
            })
            .catch(() => {
                this.setState({ state: ComponentState.ERROR })
            });
    }

    render() {
        const { locations, state } = this.state;

        if (state === ComponentState.LOADING) {
            return <Loading />;
        } else if (state == ComponentState.ERROR) {
            return <Error />
        }

        return (
            <React.Fragment>
                <Columns>
                    <Column>
                        <Button isColor="info" onClick={this.updateLocations}>Update locations</Button>
                    </Column>
                </Columns>
                <LocationList locations={locations} />
            </React.Fragment>
        );
    }

}

const Loading = () => (
    <Box>Loading...</Box>
)

const Error = () => (
    <Notification isColor="danger">Error fetching locations</Notification>
)