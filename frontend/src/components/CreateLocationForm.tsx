import * as React from 'react';
import InputRange, { Range } from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import { Columns } from 'bloomer/lib/grid/Columns';
import { Column } from 'bloomer/lib/grid/Column';
import { Field } from 'bloomer/lib/elements/Form/Field/Field';
import { Label } from 'bloomer/lib/elements/Form/Label';
import { Control } from 'bloomer/lib/elements/Form/Control';
import { Input } from 'bloomer/lib/elements/Form/Input';
import { Button } from 'bloomer/lib/elements/Button';
import { createLocationCheck } from '../api/WeatherCheckApi';
import { ComponentState } from '../util/Utils';
import { Notification } from 'bloomer/lib/elements/Notification';

interface FormState {
    city: string,
    countryCode: string,
    limit: Range,
    requestState: ComponentState
}

const ERROR_NOTIFICATION_TIMEOUT = 1000;

export default class CreateLocationForm extends React.Component<{}, FormState> {

    constructor(props: {}) {
        super(props);

        this.state = {
            city: '',
            countryCode: '',
            limit: {
                min: -10,
                max: 10
            },
            requestState: ComponentState.NOT_LOADING
        };
    }

    handleChange = (e: React.FormEvent<HTMLElement>) => {
        const target = e.target as HTMLInputElement;
        if (target.name === "city") {
            this.setState({ city: target.value });
        } else {
            this.setState({ countryCode: target.value });
        }
    }

    handleLimitChange = (limit: number | Range) => {
        if (typeof limit !== 'number') {
            this.setState({ limit });
        }
    }

    isFormValid = () => {
        return this.state.city.length > 0 && this.state.countryCode.length > 0 && this.state.limit.max > this.state.limit.min;
    }

    handleSubmit = () => {
        this.setState({ requestState: ComponentState.LOADING });
        createLocationCheck({
            location: {
                city: this.state.city,
                countryCode: this.state.countryCode
            },
            limit: {
                start: this.state.limit.min,
                end: this.state.limit.max
            }
        })
            .then(() => {
                this.setState({ requestState: ComponentState.NOT_LOADING });
            })
            .catch(() => {
                this.setUpError();
            })
    }

    setUpError = () => {
        this.setState({ requestState: ComponentState.ERROR });
        setTimeout(() => {
            this.setState({ requestState: ComponentState.NOT_LOADING });
        },
            ERROR_NOTIFICATION_TIMEOUT
        );
    }

    render() {
        return (
            <React.Fragment>
                <Columns>
                    <Column isSize="1/2">
                        <Field>
                            <Label>City</Label>
                            <Control>
                                <Input type="text" placeholder='City' name="city" value={this.state.city} onChange={this.handleChange} />
                            </Control>
                        </Field>
                        <Field>
                            <Label>Country code</Label>
                            <Control>
                                <Input type="text" placeholder='Country code' name="countryCode" value={this.state.countryCode} onChange={this.handleChange} />
                            </Control>
                        </Field>
                        <Field>
                            <Label>Limits:</Label>
                            <Control>
                                <InputRange minValue={-100} maxValue={100} value={this.state.limit} onChange={this.handleLimitChange} />
                            </Control>
                        </Field>

                    </Column>
                </Columns>
                <Columns>
                    <Column>
                        <Field isGrouped>
                            <Control>
                                <Button isColor='primary'
                                    disabled={!this.isFormValid()}
                                    isLoading={this.state.requestState === ComponentState.LOADING}
                                    onClick={this.handleSubmit}
                                >
                                    Submit
                                </Button>
                            </Control>
                        </Field>
                    </Column>
                </Columns>
                <Columns>
                    {this.state.requestState === ComponentState.ERROR ? 
                        <Notification isColor="danger">Error during creation of location check!</Notification>
                        : null}
                </Columns>
            </React.Fragment>
        )
    }

}