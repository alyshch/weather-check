import * as React from 'react';
import { LocationView, LocationForecastForDate } from '../model/Location';
import { Columns } from 'bloomer/lib/grid/Columns';
import { Column } from 'bloomer/lib/grid/Column';
import { Card } from 'bloomer/lib/components/Card/Card';
import { CardContent } from 'bloomer/lib/components/Card/CardContent';
import { Level } from 'bloomer/lib/components/Level/Level';
import { LevelItem } from 'bloomer/lib/components/Level/LevelItem';
import { Subtitle } from 'bloomer/lib/elements/Subtitle';
import { LevelLeft } from 'bloomer/lib/components/Level/LevelLeft';
import { Box } from 'bloomer/lib/elements/Box';
import { Media } from 'bloomer/lib/components/Media/Media';
import { MediaContent } from 'bloomer/lib/components/Media/MediaContent';
import { Content } from 'bloomer/lib/elements/Content';
import { Notification } from 'bloomer/lib/elements/Notification';

export interface LocationListProps {
    locations: LocationView[]
}

export function LocationList(props: LocationListProps) {

    return (
        <Columns>
            <Column>
                {props.locations.map(l => <Location location={l} />)}
            </Column>
        </Columns>
    )

}

interface LocationProps {
    location: LocationView
}

function Location({ location }: LocationProps) {
    return (
        <Box>
            <Media>
                <MediaContent>
                    <Content>
                        <p><strong>{location.location.city}</strong> <small>({location.location.countryCode})</small></p>
                        <p>Limits: from {location.limit.start} to {location.limit.end}</p>
                        {location.lastChecked ?
                            <small>Last checked: {location.lastChecked}</small>
                            : <small className="has-text-danger">Unable to fetch any forecasts</small>}
                    </Content>
                </MediaContent>
                <Level>
                    <LevelLeft>
                        {location.forecasts.map(f => <Forecast forecast={f} />)}
                    </LevelLeft>
                </Level>
            </Media>
        </Box>
    )
}

interface ForecastProps {
    forecast: LocationForecastForDate
}

function Forecast({ forecast }: ForecastProps) {
    let colorClassName;
    if (forecast.limitExceeded) {
        colorClassName = "has-text-danger";
    } else {
        colorClassName = "has-text-primary";
    }
    return (
        <LevelItem>
            <Box>
                <Content>
                    <p>{forecast.date}</p>
                    <p className={colorClassName}>{forecast.temperature} °C</p>
                </Content>
            </Box>
        </LevelItem>
    )
}