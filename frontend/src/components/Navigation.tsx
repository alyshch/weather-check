import * as React from 'react';

import { Navbar } from 'bloomer/lib/components/Navbar/Navbar';
import { NavbarMenu } from 'bloomer/lib/components/Navbar/NavbarMenu';
import { NavbarStart } from 'bloomer/lib/components/Navbar/NavbarStart';
import { NavbarItem } from 'bloomer/lib/components/Navbar/NavbarItem';
import { Link } from 'react-router-dom';
import { Container } from 'bloomer/lib/layout/Container';

interface NavigationState {
    menuActive: boolean
}

export default class Navigation extends React.Component<{}, NavigationState> {
    constructor(props: {}) {
        super(props);

        this.state = { menuActive: false };
    }

    render() {
        return (
            <Container>
                <Navbar>
                    <NavbarMenu>
                        <NavbarStart>
                            <NavbarItemLink url="/locations" text="Locations" />
                            <NavbarItemLink url="/locations/create" text="Create location" />
                        </NavbarStart>
                    </NavbarMenu>
                </Navbar>
            </Container>
        );
    }
}

interface NavbarItemLinkProps {
    url: string,
    text: string
}

const NavbarItemLink = (props: NavbarItemLinkProps) => (
    <NavbarItem render={() => <Link to={props.url} className="navbar-item">{props.text}</Link>} />
)