export enum ComponentState {
    NOT_LOADING = 1,
    LOADING,
    ERROR
};