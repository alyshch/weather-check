export interface LocationView {
    location: CityWithCountryCode,
    limit: TemperatureLimit,
    lastChecked: string,
    lastError: string,
    errorsExceededMaxErrorCount: boolean,
    forecasts: LocationForecastForDate[]
}

export interface CityWithCountryCode {
    city: string,
    countryCode: String
}

export interface TemperatureLimit {
    start: number,
    end: number
}

export interface LocationForecastForDate{
    date: string,
    limitExceeded: boolean,
    temperature: number
}

export interface CreateLocationCheckCommand{
    location: CityWithCountryCode,
    limit: TemperatureLimit
}